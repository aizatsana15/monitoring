from django.contrib import admin

from url_monitoring.models import URL


@admin.register(URL)
class UrlAdmin(admin.ModelAdmin):
    list_display = ('id', 'dt_updated', 'user', 'url', 'status', 'interval')

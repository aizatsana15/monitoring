import requests
from fake_useragent import UserAgent


def check_url(url):
    """
    function send requests to url and return response status code
    """
    ua = UserAgent()
    headers = {'User-Agent': str(ua.chrome)}
    res = requests.head(url, headers=headers)
    return res.status_code

from django.urls import path
from django.contrib.auth import views as auth_views

import url_monitoring.views as monitoring_views

urlpatterns = [
    path(
        '',
        monitoring_views.DashBoardTemplateView.as_view(),
        name='dashboard'
    ),
    path(
        'login/',
        auth_views.LoginView.as_view(template_name='registration/login.html'),
        name='login'
    ),
    path(
        'logout/',
        auth_views.LogoutView.as_view(template_name='registration/logout.html'),
        name='logout'
    ),
    path(
        'register/',
        monitoring_views.RegisterTemplateView.as_view(),
        name='register'
    ),
    path(
        'create-url/', monitoring_views.CreateUrlTemplateView.as_view(),
        name='create-url'
    ),
    path(
        'delete-url/<int:pk>/', monitoring_views.UrlDeleteTemplateView.as_view(),
        name='delete-url')
]

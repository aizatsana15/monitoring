from django.apps import AppConfig


class UrlMonitorConfig(AppConfig):
    name = 'url_monitoring'

    def ready(self):
        import url_monitoring.signals

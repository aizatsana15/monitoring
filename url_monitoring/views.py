from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, ListView, DeleteView

from url_monitoring.forms import UserRegisterForm
from url_monitoring.models import URL


__all__ = [
    'DashBoardTemplateView',
    'RegisterTemplateView',
    'CreateUrlTemplateView',
    'UrlDeleteTemplateView',
]


class DashBoardTemplateView(LoginRequiredMixin, ListView):
    """
    View display list urls of user
    """

    model = URL
    template_name = 'url_monitoring/dashboard.html'
    content_type = "text/html"
    login_url = 'login'

    def get_queryset(self):
        return URL.objects.filter(user=self.request.user).order_by('dt_created')


class RegisterTemplateView(CreateView):
    """
    View for creating user account
    """
    form_class = UserRegisterForm
    success_url = '/login/'
    template_name = 'registration/register.html'


class CreateUrlTemplateView(LoginRequiredMixin, CreateView):
    """
    View for creating URL record in DB
    """
    model = URL
    fields = ['url', 'interval']
    template_name = 'url_monitoring/url_add.html'
    login_url = '/login/'
    success_url = '/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UrlDeleteTemplateView(LoginRequiredMixin, DeleteView):
    """
    View is responsible for deleting URL from DB
    """
    model = URL
    template_name = 'url_monitoring/url_delete.html'
    content_type = "text/html"
    raise_exception = True  # Raise exception when no access instead of redirect to login page
    success_url = '/'

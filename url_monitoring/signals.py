import json

from django.db.models.signals import post_save,  pre_delete
from django.dispatch import receiver
from django_celery_beat.models import PeriodicTask, IntervalSchedule

from url_monitoring.models import URL
from url_monitoring.tasks import set_url_status


@receiver(post_save, sender=URL)
def update_url_record(sender, instance, created, **kwargs):
    if created:
        set_url_status.delay(instance.pk)

        schedule, created = IntervalSchedule.objects.get_or_create(
            every=instance.interval,
            period=IntervalSchedule.SECONDS
        )

        task_name = 'url_monitoring.tasks.set_url_status'
        PeriodicTask.objects.create(
            interval=schedule,
            name="%s_%s" % (task_name, instance.pk),
            task=task_name,
            args=json.dumps([instance.pk]),
        )


@receiver(pre_delete, sender=URL)
def delete_url_record(sender, instance, *args, **kwargs):
    task_name = 'url_monitoring.tasks.set_url_status'
    periodic_task = PeriodicTask.objects.filter(
        name="%s_%s" % (task_name, instance.pk)
    ).last()

    if periodic_task:
        periodic_task.delete()

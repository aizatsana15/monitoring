from monitoring.celery import app
from url_monitoring.models import URL

from url_monitoring.utils import check_url


@app.task
def set_url_status(url_record_id):
    url_record = URL.objects.get(id=url_record_id)
    response_status_code = check_url(url_record.url)
    if response_status_code != url_record.status:
        url_record.status = response_status_code
        url_record.save()

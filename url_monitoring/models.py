from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class URL(models.Model):
    """
    Model for storing user record of URL with interval and response status code
    """
    dt_created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Date created',
        help_text='Time of created record'
    )
    dt_updated = models.DateTimeField(
        auto_now=True,
        verbose_name='Date updated',
        help_text='Time of updated record'
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.URLField(
        verbose_name='url',
        help_text='Tracked URL'
    )
    status = models.IntegerField(
        blank=True,
        null=True,
        verbose_name='status',
        help_text='Last response HTTP status code from URL'
    )
    interval = models.FloatField(
        verbose_name='interval',
        help_text='Periodic time interval in seconds for Celery task'
    )

    def __str__(self):
        return "Record with url {}".format(self.url)

    def get_absolute_url(self):
        return reverse('dashboard')

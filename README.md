## Сontents
* [General info](#general-info)
* [Setup](#setup)

## General info

Monitoring app help you add/remove url for monitoring and check their status. 
Set interval for retrieving response from sites in seconds.

## Setup  
To run this project:
```
$ git clone https://gitlab.com/aizatsana15/monitoring.git
$ pip3 install -r requirements.txt
$ python3 manage.py migrate
```

For work with celery:

If you don`t want to install some services (like RabbitMQ, Redis, etc.) you could use Docker. 
For installation instructions refer to the Docker [here](https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru).

1 Install && run RabbitMQ
```
$ docker pull rabbitmq 
$ docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```

2 Add queue "celery" in RabbitMQ
```
You can do this via web page: http://localhost:15672/  (default login/password: guest)
```


3 Start Celery
```
$  celery -A monitoring worker --beat --scheduler django --loglevel=info
```
